"use strict";

var stickyHeader = function(){
    var mobileContactIcon,
        mobileMenu,
        header;

    function init() {
        mobileContactIcon = document.getElementsByClassName('ps-header__mobile-contact-icon')[0];
        mobileMenu = document.getElementsByClassName('ps-header__mobile-menu-expanded')[0];
        header = document.getElementById('header');
        bindEvents();
    }

    function bindEvents() {
        window.onscroll = function() {
           window.pageYOffset > 200 ? header.classList.add('reduced') : header.classList.remove('reduced');
        }

        mobileContactIcon.addEventListener('click', function(){
            if(mobileMenu.classList.contains('active')) {
                mobileMenu.classList.remove('active');
            } else {
                mobileMenu.classList.add('active');
            }
        })
    }

    return {
        init: init
    }
}();

window.onload = function() {
    stickyHeader.init();
}
